// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameStateBase.h"
#include "MainGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class SGD240_SPIKE_4_API AMainGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	// Constructor
	AMainGameStateBase();

	// Score functions
	UFUNCTION(BlueprintCallable, Category = "GameState")
		void ResetScore();

	UFUNCTION(BlueprintCallable, Category = "GameState")
		void AddScore();

private:
	// Score Variable
	UPROPERTY(EditAnywhere)
		int CollectibleScore;
};
