// Fill out your copyright notice in the Description page of Project Settings.

#include "sgd240_spike_4.h"
#include "MainPlayerPawn.h"


// Sets default values
AMainPlayerPawn::AMainPlayerPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set this pawn to be controlled by the lowest-numbered player
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	// Create a collider and set it up
	OurCollider = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	OurCollider->SetSphereRadius(51, false);
	OurCollider->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	OurCollider->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Overlap);
	OurCollider->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Overlap);
	OurCollider->SetCollisionObjectType(ECC_Pawn);

	// Set our collider to be the root component
	RootComponent = OurCollider;

	// Scale the player object down by default
	RootComponent->SetWorldScale3D(FVector(0.3, 0.3, 0.3));

	// Set default speed
	SpeedMultiplier = 4;

	// Movement enabled by default
	DisableMovement = false;

	// Do not ignore DisableMovement by default
	IgnoreDisableMovement = false;

	// Create a camera and a visible object
	OurCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("OurCamera"));
	OurVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("OurVisibleComponent"));

	// Set up default camera position
	OurCameraOffset = FVector(0.0f, 0.0f, 1000);
	OurCameraRotation = FRotator(-90.0f, 0.0f, 0.0f);

	// Attach our camera and visible object to our root component. Offset and rotate the camera.
	OurCamera->SetupAttachment(RootComponent);
	OurCamera->SetRelativeLocation(OurCameraOffset);
	OurCamera->SetRelativeRotation(OurCameraRotation);

	// Attatch the visible component
	OurVisibleComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AMainPlayerPawn::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMainPlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!MovementVector.IsZero() && (!DisableMovement || IgnoreDisableMovement))
	{
		// debugging 
		//FString currentVelocityString = MovementVector.ToString();
		//UE_LOG(LogTemp, Log, TEXT("Movement vector is: %s"), *currentVelocityString);

		// Prepare the movement vector:
		// Normalize to avoid moving faster on the diagonals
		MovementVector.Normalize();
		// Set the speed according to the speed multiplier and Delta Time. Add some extra to make base speed visable.
		MovementVector = MovementVector * 100 * SpeedMultiplier * DeltaTime;
				
		// Alternative movement method:
		//FVector NewLocation = GetActorLocation() + (MovementVector * DeltaTime);		
		//SetActorLocation(NewLocation, true)

		// Create a variable to store our sweep result
		FHitResult* HitResult = new FHitResult;

		// Attempt to move
		AddActorWorldOffset(MovementVector, true, HitResult);

		// If attempt was blocked
		if (HitResult->bBlockingHit)
		{
			// Try without the X value (go directly to the side)
			FVector MovementVectorZeroX = MovementVector;
			MovementVectorZeroX.X = 0;
			AddActorWorldOffset(MovementVectorZeroX, true, HitResult);			

			// If that failed as well
			if (HitResult->bBlockingHit)
			{
				// Try without the y value (go directly forward)
				FVector MovementVectorZeroY = MovementVector;
				MovementVectorZeroY.Y = 0;
				AddActorWorldOffset(MovementVectorZeroY, true, HitResult);
			}
		}
	}
	// Clear the movement vector to avoid continuous movement
	MovementVector.X = MovementVector.Y = 0;
}

// Function to move on the X axis (forward)
void AMainPlayerPawn::Move_XAxis(float AxisValue)
{
	// Set the value of the X axis movement
	MovementVector.X = FMath::Clamp(AxisValue, -1.0f, 1.0f);	
}

// Function to move on the Y axis (right)
void AMainPlayerPawn::Move_YAxis(float AxisValue)
{
	// Set the value of the Y axis movement
	MovementVector.Y = FMath::Clamp(AxisValue, -1.0f, 1.0f);
}

// Disable the player's pawn
void AMainPlayerPawn::DisablePlayerMovement()
{
	DisableMovement = true;
}

// Toggle disable movement
void AMainPlayerPawn::ToggleIgnoreDisablePlayerMovement()
{
	// If we are ignoring disabled movement, re-enable it.
	if (IgnoreDisableMovement)
	{
		DisableMovement = false;
		IgnoreDisableMovement = false;
	}
	// Otherwise, disable it
	else if (!IgnoreDisableMovement)
	{
		IgnoreDisableMovement = true;
	}

	// Log & print to screen
	UE_LOG(LogTemp, Log, TEXT("Ignore Disable Movement: %s"), IgnoreDisableMovement ? TEXT("true") : TEXT("false"));

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Cyan, FString::Printf(TEXT("Ignore Disable Movement: %s"), IgnoreDisableMovement ? TEXT("true") : TEXT("false")));
	}
}