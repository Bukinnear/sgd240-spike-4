// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "MainPlayerPawn.generated.h"

UCLASS()
class SGD240_SPIKE_4_API AMainPlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMainPlayerPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	// Our collider
	UPROPERTY(EditAnywhere)
		USphereComponent* OurCollider;
	
	// Our visible component
	UPROPERTY(EditAnywhere)
		USceneComponent* OurVisibleComponent;
	
	//Input functions
	UFUNCTION(BlueprintCallable, Category = "Pawn")
		void Move_XAxis(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = "Pawn")
		void Move_YAxis(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = "Pawn")
		void DisablePlayerMovement();

	UFUNCTION(BlueprintCallable, Category = "Pawn")
		void ToggleIgnoreDisablePlayerMovement();

private:

	// Movement variables
	FVector MovementVector;

	UPROPERTY(EditAnywhere)
		float SpeedMultiplier;

	UPROPERTY(EditAnywhere)
		bool DisableMovement;

	UPROPERTY(EditAnywhere)
		bool IgnoreDisableMovement;

	// Camera Variables
	UPROPERTY()
		UCameraComponent* OurCamera;

	UPROPERTY(EditAnywhere)
		FVector OurCameraOffset;

	UPROPERTY(EditAnywhere)
		FRotator OurCameraRotation;
};
