---
title: Spike Report
---

Core 4 – Unreal Engine Gameplay Framework
=========================================

Introduction
------------

We’ve just done some tutorials on how to use C++ and Blueprints
together, but now we need to look across the entirety of how Unreal
Engine works to understand it.

Let’s build a small project in Unreal that explores each of the areas,
and write a report that explains what is the purpose of (and
relationships between) each of the following:

Goals
-----

1.  A small Unreal Engine project, built from scratch (can use content
    from any project from the Learn Tab in the Epic Games Launcher).

    **Scope:**

    a.  The player controls a character that collides with terrain.

    b.  There are enemies that move around the level, some chase the
        player, others patrol on a set path

    c.  There are collectibles throughout the level

    d.  Colliding with an enemy disables the player’s movement

    e.  Colliding with a collectible adds it to the player’s score.

2.  The project must include the following custom classes (i.e. child
    classes):

    a.  Game Mode (using Blueprint or C++)

        i.  Sets the game to use the Pawn, Controller and GameState
            below

    b.  A Pawn or Character (C++)

        i.  Game Logic for any playable/controllable Actors goes here

        ii. There must be at least one UFUNCTION which the Controller
            can call

        iii. There must be at least one UPROPERTY which the Controller
            can read or write to

    c.  A Player Controller (Blueprints or C++)

        i.  This must use the C++ functions and variables from the
            Pawn/Character

        ii. Control logic goes here (i.e. how to convert Input for the
            Pawn to use)

        iii. It must support at LEAST: Mouse and Keyboard

        iv. Should also support one of:

            1.  Game Pad

            2.  Virtual Reality Controllers

    d.  A GameState (C++)

        i.  It should track a Score of some kind (or Timer, or similar)

Personnel
---------

In this section, list the primary author of the spike report, as well as
any personnel who assisted in completing the work.

  ------------------- -----------------
  Primary – Jared K   Secondary – N/A
  ------------------- -----------------

Technologies, Tools, and Resources used
---------------------------------------

-   Extensive use of the Unreal Documentation:
    <https://docs.unrealengine.com/latest/INT/>

-   C++ Programming Tutorials Hub:
    <https://docs.unrealengine.com/latest/INT/Programming/Tutorials/index.html>

-   C++ Class Creation Guide:
    <https://docs.unrealengine.com/latest/INT/Gameplay/ClassCreation/CodeOnly/index.html>

-   Several other web forums that, unfortunately, were not recorded.

Tasks undertaken
----------------

Since the project is too long to go into fine detail here, I will simply
describe the role of each of the files created for this project – more
details can be found in the files themselves.

1.  **MainGameMode**

    a.  The Game Mode file was created as a Blueprint File. This is a
        fairly simple file, it defines important default files to be
        used, like the Player Controller, Game State, Etc. This is a
        fairly simple file, aside from setting those defaults, it only
        resets the player’s score when the game begins.

2.  **MainPlayerController**

    a.  The Player Controller file was created as a Blueprint file – it
        receives input events defined in Project Settings -&gt; Input
        and assigns actions to them. In this case, it gets the player’s
        Pawn and casts it to MainPlayerPawn (since that’s the only thing
        we use), and calls the relevant functions needed.

3.  **MainPlayerPawn**

    a.  The playable pawn file was created in C++ - it contains
        Blueprint-exposed functions required to respond to user input
        (see point 8 in [What We Found Out](#what-we-found-out) for
        details). It also contains code for setting up the object when
        placed in the scene (this process is more extensive than
        difficult, see the code for examples). One of the most complex
        files in the project, read it for more details.

4.  **MoveToTarget**

    a.  Movement component created in blueprint, to be attached to
        either a basic enemy or collectible. It either seeks the player,
        or patrols to waypoints set by arrow components attached to the
        parent object.

5.  **BasicEnemy**

    a.  Created in blueprint. Handles collision with the player pawn and
        kills the player on contact.

6.  **Collectible**

    a.  Created in blueprint. Similar to the Basic Enemy, handles
        collision with the player object and adds score to the
        MainGameState file on contact.

7.  **GameState**

    a.  Created in C++, contains functions to add to, or reset the
        score. Currently it just prints to screen as a placeholder for a
        UI.

What We Found Out
-----------------

1.  Player object needs a mesh, and the mesh needs a material, to be
    visible.

2.  A class file IS an object. Script components similar to Unity
    scripts can be created by using a basic actor C++ or Blueprint file.

3.  How to create a new C++ file:

    a.  Content Browser -&gt; Add New -&gt; C++ File

    b.  Select the class you want to inherit from – actor for a basic
        component, Pawn for a playable object, etc.

4.  When a pawn class file is created, it’s components are attached
    entirely through code. (Which makes blueprint pawns a lot easier to
    work with).

5.  Make a new level

    a.  File -&gt; New Level (This is recommended, don’t try to edit the
        default scene)

    b.  The new scene can be made default in Edit -&gt; Project Settings
        -&gt; Maps and Modes.

6.  The Game Mode need to be set in Project Settings -&gt; Maps and
    Modes to be used. Game State, Player Controller Class, etc. are
    either defined in the same place, OR in the chose Game Mode file
    (Easier in blueprint).

7.  Inputs are defined in Project Settings -&gt; Input. These can be
    bound to function in either a blueprint script by using and event in
    Blueprints, or UInputComponent::BindKey in C++. See the
    MainPlayerController for a blueprint example.

8.  In C++ files, any property or function you want to expose outside of
    the script needs to be declared the in header file and use UPROPERTY
    or UFUNCTION to control access. See the documentation for more
    details

9.  Objects can be moved by using either Force, SetActorLocation, or
    AddWorldOffset – this project used the latter. The object using
    these needs to have a Primitive component (a collider) as the root
    component to use the sweep function (which detects collisions before
    moving). See the MainPlayerPawn class for a C++ example of this, or
    the MoveToTarget class for Blueprint.

10. Collision is handled by collider components, either by using a
    preset, or by manually assigning responses to the different
    collision channels. This is much easier in Blueprints. See the Basic
    Enemy class for a blueprint implementation, and the Main Player Pawn
    for C++.

\[Optional\] Open Issues/risks
------------------------------

1.  The manner in which the AI moves is not ideal and would need
    refining if it were to be needed in future projects. Look into Seek
    Behavior if that is the case. Alternatively, look up arrive behavior
    for a similar waypoint system and consider using SetPlayerLocation
    instead of AddWorldOffset.

 \[Optional\] Recommendations
-----------------------------

For future projects, it would be prudent to research Seek Behavior for
enemies/collectibles.
